Taxonomy_form_block
------------------------
Enables the presentation of taxonomy creation forms in blocks. This is
particularly useful for including forms on panels.

Installation:
-------------------------
Simply go to the module administration page and enable the module and 
click Save Configuration.

Instructions:
--------------------------
1.Go to the admininstration page of the vocabulary from which the 'add' 
  form should be made available as a block.
2.Go to the 'Form block' section.
3.Check the box "[] Enable data entry from a block" as it says in the 
  description this will 'make the entry form for this vocabulary available
  as a block.'
4.Visit the block administration page to enable and configure the block 
  containing the form.
5.Note that only users with permission to create the given vocabulary will
  see the block. Adjust user permissions accordingly.
